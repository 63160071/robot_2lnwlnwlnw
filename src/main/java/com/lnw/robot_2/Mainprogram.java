/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.robot_2;

/**
 *
 * @author Maintory_
 */
import java.util.*;
public class Mainprogram {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        Tablemap map = new Tablemap(10, 10);
        Robot robot = new Robot(2, 2, 'X', map);
        Bomb bomb = new Bomb(5, 5);
        map.setRobot(robot);
        map.setBomb(bomb);
        while(true){
            map.showMap();   
            String str = kb.next();
            char direction = str.charAt(0);
            if(direction == 'q'){
                System.out.println("bye");
                break;
            }
            robot.walk(direction);
        }
    }
}
