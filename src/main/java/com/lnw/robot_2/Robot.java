package com.lnw.robot_2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Maintory_
 */
public class Robot {

    private int x;
    private int y;
    private char symbol;
    private Tablemap map;

    public Robot(int x, int y, char symbol, Tablemap map) {
        this.x = x;
        this.y = y;
        this.symbol = symbol;
        this.map = map;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }

    public boolean walk(char direction) {
        switch (direction){
            case 'N':
            case 'w':    
               if(map.inMap(x,y-1)){
                y = y-1;
                }
                break;
            case 'S':
            case 's': 
               if(map.inMap(x,y+1)){
                y = y+1;
                }
                break;
            case 'W':
            case 'd': 
               if(map.inMap(x-1,y)){
                x = x-1;
                }
                break;
            case 'E':
            case 'a': 
                if(map.inMap(x+1,y)){
                x = x+1;
                }
                break;
            default:
                return false;
        }
        if(map.isBomb(x, y)){
            System.out.println("BOOM !");
        }
        return false;
    }


    public boolean isOn(int x,int y) {
        return this.x == x && this.y == y;
    }

}
